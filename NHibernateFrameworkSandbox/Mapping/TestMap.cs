﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHibernateFrameworkSandbox.Mapping
{
    public class TestMap : ClassMap<Test>
    {
        public TestMap()
        {
            this.Table("tbl_test");
            this.Id(x => x.Id);
            this.Map(x => x.Value);
        }
    }
}
